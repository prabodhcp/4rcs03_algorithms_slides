/***************************************************************************
*File		: 07BubbleSort.c
*Description	: Program to implement Sorting by Counting Algorithm
*Author		: Prabodh C P
*Compiler	: gcc compiler, Ubuntu 8.10
*Date		: 7 September 2010
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>

/***************************************************************************
*Function	: main
*Input parameters: none
*RETURNS	:	0 on success
***************************************************************************/

int main(void)
{
	int iNum, i, j, iaArr[10], iaCount[10], iaSort[10];

	printf("\nEnter no of elements\n");
	scanf("%d",&iNum);

	printf("\nEnter the elements\n");
	for(i=0;i<iNum;i++)
		scanf("%d",&iaArr[i]);

	for(i=0;i<iNum;i++)
		iaCount[i]=0;		

	for(i=0;i<iNum-1;i++)
	{
		for(j=i+1;j<iNum;j++)
		{
			if(iaArr[i] < iaArr[j])
				iaCount[j]++;
			else
				iaCount[i]++;
		}

	}

	for(i=0;i<iNum;i++)
	{
		iaSort[iaCount[i]] = iaArr[i];
	}

	for(i=0;i<iNum;i++)
		printf("%d --> %d\n",iaArr[i], iaCount[i]);
			
	
	printf("\nThe Sorted array is \n");

	for(i=0;i<iNum;i++)
		printf("%d\t",iaSort[i]);

	printf("\n");
	return 0;
}


